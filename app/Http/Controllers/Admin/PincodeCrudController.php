<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PincodeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Pincode;
use App\Models\User_entry;


/**
 * Class PincodeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PincodeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Pincode::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/pincode');
        CRUD::setEntityNameStrings('pincode', 'pincodes');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        //CRUD::setFromDb(); // columns
        $this->crud->addFilter([
            'name'  => 'is_available',
            'type'  => 'select2',
            'label' => 'Is Available'
          ], [
            0 => 'No',
            1 => 'Yes',

          ], function($value) { // if the filter is active
             $this->crud->addClause('where', 'is_available', $value);
          });

        $this->crud->removeButton('show');

        CRUD::addColumn(['name' => 'pincode', 'type' => 'text']);
        CRUD::addColumn(['name' => 'city', 'type' => 'text']);
        CRUD::addColumn(['name' => 'state', 'type' => 'text']);
        CRUD::addColumn(['name' => 'region', 'type' => 'text']);
        CRUD::addColumn(['name' => 'is_available', 'type' => 'boolean']);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PincodeRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function getPincodes(Request $request){
        try{
            if($request){
                if($request->header('api-key')=='aa542fa9-1007-4e00-9645-93fcae1c039f'){
                    $pincode = $request->pincode;
                    if(strlen($pincode)==6){
                        $result=Pincode::where('pincode',$pincode)->first();
                        if($result && $result['is_available']==1){
                            $result['status']='successful';
                            return response($result);
                        }else{
                            $res['status']='failed';
                            $res['reason']='Pincode not Available';
                            return response($res);

                        }
                    }else{
                        $res['status']='failed';
                        $res['reason']='Invalid pincode';
                        return response($res);
                    }
                }
            }
        }catch(\Exception $e){

            return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
        }
    }

    public function saveData(Request $request){
        try{
            if($request){
                if($request->header('api-key')=='aa542fa9-1007-4e00-9645-93fcae1c039f'){
                    $fullname = $request->fullname;
                    $email = $request->email;
                    $mob = $request->mob;
                    $product = $request->product;
                    $pincode = $request->pincode;
                    $utm_source=$request->utm_source;
				$utm_medium=$request->utm_medium;
				$utm_campaign=$request->utm_campaign;
				$utm_id=$request->utm_id;
				$utm_term=$request->utm_term;
				$utm_content=$request->utm_content;
                    $entry=User_entry::create([
                        'fullname' =>$fullname,
                        'email' => $email,
                        'mobile_number'=>$mob,
                        'pincode'=>$pincode,
                        'product'=>$product,
                        "utm_source"=>$utm_source,
                        "utm_medium"=>$utm_medium,
                        "utm_campaign"=>$utm_campaign,
                        "utm_id"=>$utm_id,
                        "utm_term"=>$utm_term,
                        "utm_content"=>$utm_content,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    return response($entry);
                }
            }
        }catch(\Exception $e){

            return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
        }
    }

    public function updateData(Request $request){
        try{
            if($request){
                if($request->header('api-key')=='aa542fa9-1007-4e00-9645-93fcae1c039f'){
                    $pincode = $request->pincode;
                    $city = $request->city;
                    $state = $request->state;
                    $region = $request->region;
                    $is_available = $request->is_available;
                    $is_entered = Pincode::where('pincode',$pincode)->first();
                    if($is_entered){
                        $entry=Pincode::where('pincode',$pincode)->update([
                            'city' =>$city,
                            'state' => $state,
                            'region'=>$region,
                            'is_available' => $is_available,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                        $res['status']='updated';
                    }else{
                        $entry=Pincode::create([
                            'pincode' =>$pincode,
                            'city' =>$city,
                            'state' => $state,
                            'region'=>$region,
                            'is_available' => $is_available,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                        $res['status']='created';

                    }
                    return response($res);
                }
            }
        }catch(\Exception $e){

            return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
        }
    }


}
