<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\User_entryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class User_entryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class User_entryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User_entry::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user_entry');
        CRUD::setEntityNameStrings('user_entry', 'User Entries');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
       // CRUD::setFromDb(); // columns
       $this->crud->enableExportButtons();
       $this->crud->disableResponsiveTable();
        CRUD::addColumn(['name' => 'fullname', 'type' => 'text']);
        CRUD::addColumn(['name' => 'email', 'type' => 'email']);
        CRUD::addColumn(['name' => 'mobile_number', 'type' => 'phone']);
        CRUD::addColumn(['name' => 'pincode', 'type' => 'text']);
        CRUD::addColumn(['name' => 'product', 'type' => 'text']);
        CRUD::addColumn(['name' => 'utm_source', 'type' => 'text',"label" => 'Campaign Source']);
        CRUD::addColumn(['name' => 'created_at', 'type' => 'datetime',"label" => 'Created Date','format' => 'MMMM D, YYYY h:mm a']);
        $this->crud->removeButton('delete');
        $this->crud->removeButton('create');
        $this->crud->removeButton('update');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(User_entryRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
